#!/bin/bash

containerName=skald
version=latest

cd docker

wget https://gitlab.com/Snogard/skald-server/-/archive/master/skald-server-master.zip -O skald-server.zip
unzip skald-server.zip
mv skald-server-master skald-server

tag="$containerName:$version"
dockerFile=Dockerfile

podman build -f $dockerFile -t $tag