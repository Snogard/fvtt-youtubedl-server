import path from "node:path";
import fs from "node:fs";
import { SimpleLogger } from "../logger.js";

const logger = new SimpleLogger();

export class FileServerHandler {
    #downladFolder = null;
    constructor(data){
        this.#downladFolder = data.downloadFolder;
    }

    handleFileServer(request,response){
    
        let requestUrl = request.url.substring(1,request.url.length);

        let extname = path.extname(requestUrl);
        let contentType = 'text/html';
        switch (extname) {
            case '.ogg':
                contentType = 'audio/ogg';
                break;
        }

        

        try {
            let filePath = decodeURIComponent(this.#downladFolder +"/"+ requestUrl);
            logger.log("requesting: "+filePath);

            if(fs.existsSync(filePath)){
            
                if(fs.lstatSync(filePath).isFile())
                {
                    fs.readFile(filePath, function(error, content) {
                        if (error) {
                            if(error.code == 'ENOENT'){
                                fs.readFile('./404.html', function(error, content) {
                                    response.writeHead(200, { 'Content-Type': contentType });
                                    response.end(content, 'utf-8');
                                });
                            }
                            else {
                                response.writeHead(500);
                                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                                response.end(); 
                            }
                        }
                        else {
                            response.setHeader('Access-Control-Allow-Origin', '*');
                            response.writeHead(200, { 'Content-Type': contentType });
                            response.end(content, 'utf-8');
                        }
                    });
                }
                else if(fs.lstatSync(filePath).isDirectory())
                {
                    let html='<html><meta charset="UTF-8"><body><h2>'+filePath.replace(this.#downladFolder,"")+'</h2>'
                    html+=this.#createHtmlLink("..","folder");
                    
                    fs.readdirSync(filePath).forEach(entry => {
                        let lstat = fs.lstatSync(filePath + entry);
                        html+=this.#createHtmlLink(entry, lstat.isFile() ? "file" : "folder");
                    });

                    html+="</body></html>"
                    response.writeHead(200, {'Conten-Type': "text/html"});
                    response.end(html, "utf-8");
                }
            }
            else{
                response.writeHead(404);
                response.end(); 
            }
        }
        catch(error)
        {
            response.writeHead(500);
            response.end();
        }
    }

    #createHtmlLink(entry, type){
        switch(type){
            case "folder":
                return '<a href="'+encodeURIComponent(entry+"/").replace("%2F","/")+'">'+"./"+entry+'</a><br>';
            case "file":
                return '<a href="'+encodeURIComponent(entry).replace("%2F","/")+'">'+entry+'</a><br>';
        }
    }
}