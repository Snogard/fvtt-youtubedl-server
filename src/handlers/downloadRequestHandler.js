import fs from "node:fs";

import PQueue from 'p-queue';
import yt from "youtube-dl-exec";
import ffmpeg from "fluent-ffmpeg";
import { SimpleLogger } from "../logger.js";

const logger = new SimpleLogger();

export class DownloadRequestHandler {
    #downloadFolder = null;
    #configs = null;

    #downloadQueues = {}

    constructor(data){
        this.#downloadQueues = {};
        this.#downloadFolder = data.configs.downloadFolder;
        this.#configs = data.configs;
    }

    enqueueRequest(request,response,body){
        logger.log("Enqueueing download request from " + ((request.headers['x-forwarded-for'] || '').split(',').pop().trim() || request.socket.remoteAddress));

        let postData = null;

        try {
            postData = JSON.parse(body);
        }
        catch (error){
            this.#replyError(response,400,"json is invalid");
            return;
        }

        let user = this.#configs.getUserConfig(postData.username);
        if (!user) {
            this.#replyError(response,401,"Username denied");
            return;
        }
        else{
            if (!this.#downloadQueues[user.name]){
                this.#downloadQueues[user.name] = new PQueue({concurrency:1});
            }

            this.#downloadQueues[user.name].add(() => this.#handleDownloadRequest(request,response,postData));
        }
    }

    async #handleDownloadRequest(request,response,postData)
    {
        let downloadData = null;
        logger.log("Handling download request from " + ((request.headers['x-forwarded-for'] || '').split(',').pop().trim() || request.socket.remoteAddress));

        logger.log(postData);
        if (postData.url){
            let username = postData.username ? postData.username : "default"
            let user = this.#configs.getUserConfig(postData.username);
            if(user)
            {
                logger.log("username: "+username +" -> "+user.name);
                downloadData = await this.#download(postData.url,user.name);
            }
            else{
                this.#replyError(response,401,"Username denied");
                return;
            }
        }

        if (!downloadData.error){

            let responseData = {};
            if (!downloadData.playlistName){
                responseData = {
                    id: downloadData.files[0].id,
                    original_url: downloadData.files[0].original_url,
                    url: encodeURIComponent(downloadData.files[0].path.substring(this.#downloadFolder.length,downloadData.files[0].path.length)),
                    title: downloadData.files[0].title,
                }
            }
            else{
                responseData.playlistName = downloadData.playlistName;
                responseData.playlistUrl = downloadData.playlistUrl;
                responseData.soundtracks = []
                for (let i = 0; i < downloadData.files.length; i++){
                    let soundtrack = {
                        id: downloadData.files[i].id,
                        original_url: downloadData.files[i].original_url,
                        url: encodeURIComponent(downloadData.files[i].path.substring(this.#downloadFolder.length,downloadData.files[i].path.length)),
                        title: downloadData.files[i].title,
                    };
                    responseData.soundtracks.push(soundtrack);
                }
            }

            response.setHeader('Access-Control-Allow-Origin', '*');
            response.writeHead(200, {'Content-Type': 'text/json'});
            response.end(JSON.stringify(responseData));
        }
        else{
            this.#replyError(response,400,downloadData.error);
        }
    }

    async #download(url,username)
    {
        let downloadInfo={};
        let result = {};
        result.paths = [];

        if(!url || !username){
            return {error: "Missing at least one parameter"}
        }

        // get json
        logger.log("Retrieving url information")
        try {
            await yt(url,{
                dumpSingleJson: true,
            }).then(output => downloadInfo.info = output);
        }catch(error){
            logger.log(error)
            return  {error: "Failed to retrieve information"};
        }

        downloadInfo.sources = [];
  
        try {
        
            if(!downloadInfo.info.entries){ // not is playlist
                downloadInfo.folder = this.#downloadFolder+"/"+username;

                downloadInfo.sources.push({
                    title: downloadInfo.info.title,
                    url: downloadInfo.info.original_url,
                    fileName: downloadInfo.info.requested_downloads[0]._filename,
                    filePath: downloadInfo.folder+"/"+downloadInfo.info.requested_downloads[0]._filename,
                    id: downloadInfo.info.id,
                });
            }
            else{
                downloadInfo.playlistName = downloadInfo.info.title;
                downloadInfo.folder = this.#downloadFolder+"/"+username+"/"+downloadInfo.playlistName;

                for(let index in downloadInfo.info.entries)
                {
                    let entry = downloadInfo.info.entries[index];
                    if (entry){
                        downloadInfo.sources.push({
                            title: entry.title,
                            url: entry.original_url,
                            fileName: entry.requested_downloads[0]._filename,
                            filePath: downloadInfo.folder+"/"+entry.requested_downloads[0]._filename,
                            id: entry.id,
                        });
                    }
                    else{
                        logger.log("This is strange, entry '"+index+"'not found in "+url);
                    }
                }
                result.playlistName = downloadInfo.playlistName;
                result.playlistUrl = url;
            }
        }catch(error){
            logger.log(error);
            return {error: "ERROR: some information is missing form "+url };
        }

        result.files = [];
        
        for(let index in downloadInfo.sources)
        {
            let source = downloadInfo.sources[index];
            let input = this.#changeExtension(source.filePath,"flac");
            let output = this.#changeExtension(source.filePath,"ogg");

            if (!fs.existsSync(output)){
                // downloading
                logger.log("Downloading: "+source.url);
                try {
                    await yt(source.url, {
                        extractAudio: true,
                        audioFormat: "flac",
                        paths: downloadInfo.folder,
                    }).then(output => logger.log(output));
                } catch (error){
                    logger.log(error);
                    return {error: "Failed to download video or playlist"}
                }

                //convert       
                logger.log("Converting to ogg: "+input);
                try {
                    await this.#ffmpegConvert(input,output);
                    fs.unlinkSync(input);
                } catch(error) {
                    logger.log(error);
                    return {error: "Failed to convert file"};
                }
            }
            else{
                logger.log("File "+output+" already exists, skipping");
            }

            result.files.push({
                title: source.title,
                id: source.id,
                original_url: source.url,
                path: output,
            }); 
        }

        return result;
    }

    #changeExtension(filePath,extension){
        let lastIndex = filePath.lastIndexOf(".");
        filePath = filePath.substring(0, lastIndex);
        return filePath+"."+extension
    }

    #ffmpegConvert(input,output){
        return new Promise((resolve,reject) => {
            ffmpeg(input)
            .output(output)
            .on('end', function () {
                resolve();
            }).on('error', function (err) {
                logger.log('error: ', err.code, err.msg);
                reject();
            }).run();
        });
    }

    #replyError(response,errorCode,msg){
        logger.log(msg);
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.writeHead(errorCode,{'Content-Type': 'text/json'});
        response.end(JSON.stringify({message:msg}));
    }
}