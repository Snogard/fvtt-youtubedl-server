import http from "node:http";
import fs from "node:fs";

import { DownloadRequestHandler } from "./handlers/downloadRequestHandler.js";
import { FileServerHandler } from "./handlers/fileServerHandler.js";
import { ServerConfig } from "./config.js";
import { Env } from "./env.js";
import { SimpleLogger } from "./logger.js";


const configs = new ServerConfig(Env.CONFIG_PATH+"/config.json");
const downloadFolder = configs.downloadFolder;

const downloadHandler = new DownloadRequestHandler({configs: configs});
const fileServerHanlder = new FileServerHandler({downloadFolder: downloadFolder});

const port = configs.port;
const host = configs.host;

const logger = new SimpleLogger();

function openServer(){
    if(!fs.existsSync(downloadFolder))
    {
        fs.mkdirSync(downloadFolder);
    }
    const server = http.createServer(function(request, response){
        console.log("\n");
        logger.log("Handling request from " + ((request.headers['x-forwarded-for'] || '').split(',').pop().trim() || request.socket.remoteAddress));
        switch(request.method) {
            case "POST":
                logger.log('POST')
                var body = ''
                request.on('data', function(data) {
                    body += data
                    logger.log('Partial body: ' + body)
                })

                switch(request.url){
                    case "/api/download":
                        request.on('end', function() {
                            logger.log('Body: ' + body)
                            downloadHandler.enqueueRequest(request,response,body);
                        })
                        break;
                    default:
                        request.on('end', function (){
                            response.writeHead(404);
                            response.end();
                        })
                        break;
                }        
                break;

            case "GET":
                    logger.log('GET')
                    fileServerHanlder.handleFileServer(request,response);
                break;
        }
        
    });

    server.listen(port,host);
    logger.log(`Listening at http://${host}:${port}`);
}


openServer();
