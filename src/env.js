import fs from "node:fs";

export class Env{
    static #configPath;
    static #downloadPath;

    static get CONFIG_PATH(){
        if(!this.#configPath){
            if(!fs.existsSync(process.env.SKALD_CONFIG_PATH)){
                this.#configPath=".";
            }
            else{
                this.#configPath = process.env.SKALD_CONFIG_PATH;
            }
        }
        return this.#configPath;
    }

    static get DOWNLOAD_PATH(){
        if(!this.#downloadPath){
            if(!fs.existsSync(process.env.SKALD_DOWNLOAD_PATH)){
                this.#downloadPath="./downloads";
            }
            else{
                this.#downloadPath = process.env.SKALD_DOWNLOAD_PATH;
            }
        }
        return this.#downloadPath;
    }

}