import fs from "node:fs"
import bytes from "bytes"
import { Env } from "./env.js";
import { SimpleLogger } from "./logger.js";

const logger = new SimpleLogger();

export class ServerConfig{
    #configs = null;

    get port(){
        return this.#configs.port;
    }

    get host(){
        return this.#configs.host;
    }

    get downloadFolder()
    {
        return this.#configs.downloadFolder;
    }

    constructor(configPath){
        if(fs.existsSync(configPath)){
            this.#configs = JSON.parse(fs.readFileSync(configPath));
        }
        else{
            this.#configs = {};
        }
        
        this.#configs = this.#validateConfig();
        logger.log("Loaded configs:");
        logger.log(this.#configs);
    }
    
    #validateConfig(){
        let configs = this.#configs;
        if(configs){
            if(configs.port == undefined){
                configs.port = 8080;
            }
            if(configs.host == undefined){
                configs.host = "0.0.0.0";
            }
            if(configs.anyUser == undefined){
                configs.anyUser = false;
            }
            if(configs.downloadFolder == undefined){
                configs.downloadFolder = Env.DOWNLOAD_PATH;
            }

            switch(configs.defaultCacheSize){
                case "unlimited":
                    break;
                case undefined: 
                    configs.defaultCacheSize = this.textToMegabytes("100MB");
                    break;
                default:
                    configs.defaultCacheSize = this.textToMegabytes(configs.defaultCacheSize);
            }
            
            //TODO: default cache retention;
            if(configs.users == undefined){
                configs.users = {};
                configs.users.default = {};
            }

            for(let userName in configs.users){
                switch(configs.users[userName].cacheSize){
                    case "unlimited":
                        break;
                    case undefined:
                        configs.users[userName].cacheSize = configs.defaultCacheSize;
                        break;
                    default:
                        configs.users[userName].cacheSize = this.textToMegabytes(configs.users[userName].cacheSize);

                }
                //TODO cache retention;
            }
        }
        else{
            logger.log("config undefined");
        }

        return configs;
    }

    getUserConfig(userName){
        let user = null;

        if(this.#configs.users[userName])
        {
            user = this.#configs.users[userName];
            user.name = userName
            return user;
        }
        else if (this.#configs.anyUser)
        {
            this.#configs.users[userName] = {};
            this.#configs.users[userName].cacheSize = this.#configs.defaultCacheSize;
            user = this.#configs.users[userName];
            user.name = userName
            return user;
        }
        else if (this.#configs.users["default"])
        {
            user = this.#configs.users["default"];
            user.name = "default";
            return user;
        }    

        return;
    }

    textToMegabytes(text){
        let number = bytes.parse(text);
        number = Math.floor(number/1024/1024);

        if (number == 0){
            throw new Error("Malformed bytes expression");
        }

        return number;
    }
}