export class SimpleLogger{
    log(message){
        console.log("["+new Date(Date.now()).toISOString()+"] "+message);
    }
}