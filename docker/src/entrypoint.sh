#!/bin/sh

node "$@" &
pid=$!

trap "echo 'Exiting...'; kill $pid" TERM


wait $pid

exit 0